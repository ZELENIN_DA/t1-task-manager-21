package ru.t1.dzelenin.tm.exception.user;

public class ExistsEmailException extends AbstractUserException {

    public ExistsEmailException() {
        super("Error! Email alredy exists...");
    }

    public ExistsEmailException(String email) {
        super("Error! Email '" + email + "'alredy exists...");
    }

}
