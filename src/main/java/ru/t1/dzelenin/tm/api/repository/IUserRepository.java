package ru.t1.dzelenin.tm.api.repository;

import ru.t1.dzelenin.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    User findByLogin(String login);

    User findByEmail(String email);

    Boolean isLoginExist(String login);

    Boolean isMailExist(String email);

}

