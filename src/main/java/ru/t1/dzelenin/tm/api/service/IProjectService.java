package ru.t1.dzelenin.tm.api.service;

import ru.t1.dzelenin.tm.enumerated.Sort;
import ru.t1.dzelenin.tm.enumerated.Status;
import ru.t1.dzelenin.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService extends IUserOwnerService<Project> {

    Project add(String userId, Project project);

    Project changeProjectStatusIndex(String userId, Integer index, Status status);

    Project changeProjectStatusId(String userId, String id, Status status);

    void clear(String userId);

    Project create(String userId, String name, String description);

    Project create(String userId, String name);

    List<Project> findAll(String userId);

    List<Project> findAll(String userId, Comparator comparator);

    List<Project> findAll(String userId, Sort sort);

    Project remove(String userId, Project project);

    Project findOneById(String userId, String id);

    Project findOneByIndex(String userId, Integer index);

    Project updateById(String userId, String id, String name, String description);

    Project updateByIndex(String userId, Integer index, String name, String description);

    Project removeById(String userId, String id);

    Project removeByIndex(String userId, Integer index);

}


