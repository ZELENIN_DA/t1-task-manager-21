package ru.t1.dzelenin.tm.api.service;

import ru.t1.dzelenin.tm.api.repository.IRepository;
import ru.t1.dzelenin.tm.enumerated.Sort;
import ru.t1.dzelenin.tm.model.AbstractModel;

import java.util.List;


public interface IService<M extends AbstractModel> extends IRepository<M> {

    List<M> findAll(final Sort sort);

}

