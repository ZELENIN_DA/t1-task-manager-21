package ru.t1.dzelenin.tm.command.project;

import ru.t1.dzelenin.tm.enumerated.Status;
import ru.t1.dzelenin.tm.util.TerminalUtil;

public final class ProjectStartByIndexCommand extends AbstractProjectCommand {

    @Override
    public void execute() {
        System.out.println("[START PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final String userId = getUserId();
        getProjectService().changeProjectStatusIndex(userId, index, Status.IN_PROGRESS);
    }

    @Override
    public String getName() {
        return "project-start-by-index";
    }

    @Override
    public String getDescription() {
        return "Start project by index.";
    }

    @Override
    public String getArgument() {
        return null;
    }

}
