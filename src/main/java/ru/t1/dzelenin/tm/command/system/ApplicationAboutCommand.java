package ru.t1.dzelenin.tm.command.system;

public class ApplicationAboutCommand extends AbstractSystemCommand {

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("name: Daniil Zelenin");
        System.out.println("email: dzelenin@t1-consulting.ru");
    }

    @Override
    public String getName() {
        return "about";
    }

    @Override
    public String getDescription() {
        return "Show developer info.";
    }

    @Override
    public String getArgument() {
        return "-a";
    }
}
