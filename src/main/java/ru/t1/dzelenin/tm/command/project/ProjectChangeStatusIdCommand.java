package ru.t1.dzelenin.tm.command.project;

import ru.t1.dzelenin.tm.enumerated.Status;
import ru.t1.dzelenin.tm.util.TerminalUtil;

import java.util.Arrays;

public final class ProjectChangeStatusIdCommand extends AbstractProjectCommand {

    private final String NAME = "project-change-status-by-id";

    private final String DESCRIPTION = "Change project by id.";

    @Override
    public void execute() {
        System.out.println("[CHANGE PROJECT STATUS BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        getProjectService().changeProjectStatusId(getUserId(), id, status);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}

