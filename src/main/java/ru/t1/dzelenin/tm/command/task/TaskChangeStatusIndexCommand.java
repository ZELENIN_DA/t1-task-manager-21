package ru.t1.dzelenin.tm.command.task;

import ru.t1.dzelenin.tm.enumerated.Status;
import ru.t1.dzelenin.tm.util.TerminalUtil;

import java.util.Arrays;

public final class TaskChangeStatusIndexCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[CHANGE TASK STATUS BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        final String userId = getUserId();
        getTaskService().changeTaskStatusIndex(userId, index, status);
    }

    @Override
    public String getName() {
        return "task-complete-by-index";
    }

    @Override
    public String getDescription() {
        return "Change task status by index.";
    }

    @Override
    public String getArgument() {
        return null;
    }

}
