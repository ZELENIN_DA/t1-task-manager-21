package ru.t1.dzelenin.tm.command.task;

import ru.t1.dzelenin.tm.enumerated.Status;
import ru.t1.dzelenin.tm.util.TerminalUtil;

public final class TaskCompleteByIdCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[COMPLETE TASK BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final String userId = getUserId();
        getTaskService().changeTaskStatusId(userId, id, Status.COMPLETED);
    }

    @Override
    public String getName() {
        return "task-complete-by-id";
    }

    @Override
    public String getDescription() {
        return "Complete task by id.";
    }

    @Override
    public String getArgument() {
        return null;
    }

}
